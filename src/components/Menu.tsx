import {
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
} from '@ionic/react';

import { useLocation } from 'react-router-dom';
import { calendarClearOutline, calendarClearSharp, calendarOutline, calendarSharp, logInOutline, logInSharp, logOutOutline, logOutSharp, peopleOutline, peopleSharp } from 'ionicons/icons';
import { useAuth } from '../core/auth';
import { Routes } from '../Routes';

interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
}

const anonymousUserNavigation: AppPage[] = [
  {
    title: 'Login',
    url: Routes.Login,
    iosIcon: logInOutline,
    mdIcon: logInSharp
  },
  {
    title: 'Create an account',
    url: Routes.CreateAnAccount,
    iosIcon: peopleOutline,
    mdIcon: peopleSharp
  },
];

const clientUserNavigation: AppPage[] = [
  {
    title: 'Schedule an appointment',
    url: Routes.ScheduleTime,
    iosIcon: calendarOutline,
    mdIcon: calendarSharp
  },
  {
    title: 'My appointments',
    url: Routes.MyAppointments,
    iosIcon: calendarClearOutline,
    mdIcon: calendarClearSharp
  },
  {
    title: 'Logout',
    url: Routes.Logout,
    iosIcon: logOutOutline,
    mdIcon: logOutSharp
  },
];

const providerUserNavigation: AppPage[] = [
  {
    title: 'Manage schedule',
    url: Routes.ProviderScheduleAdmin,
    iosIcon: logInOutline,
    mdIcon: logInSharp
  },
  {
    title: 'Logout',
    url: Routes.Logout,
    iosIcon: logOutOutline,
    mdIcon: logOutSharp
  },
];

export const Menu: React.FC = () => {
  const location = useLocation();
  const auth = useAuth();

  const appPages = (() => {
    if (auth.currentUser?.type === 'client') {
      return clientUserNavigation;
    }
    if (auth.currentUser?.type === 'provider') {
      return providerUserNavigation;
    }
    return anonymousUserNavigation;
  })();

  return (
    <IonMenu contentId="main" type="overlay" style={{ maxWidth: 240 }}>
      <IonContent>
        <IonList id="inbox-list">
          <IonListHeader>
            <img src="https://onboard.henrymeds.com/henry_logo_full_black.png" alt="HenryMeds" style={{ maxHeight: 50, marginBottom: 10 }} />
          </IonListHeader>
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem className={location.pathname === appPage.url ? 'selected' : ''} routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                  <IonIcon aria-hidden="true" slot="start" ios={appPage.iosIcon} md={appPage.mdIcon} />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}
        </IonList>
      </IonContent>
    </IonMenu>
  );
};
