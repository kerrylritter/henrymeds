import { IonDatetimeButton, IonModal, IonDatetime } from '@ionic/react';

export const getDefaultDateString = () => new Date().toISOString().split('T')[0];
export const DateSelector: React.FC<{ defaultValue: string; onChange: (date: string) => any }> = (props) => {
  return <>
    <IonDatetimeButton datetime="date"></IonDatetimeButton>
    <IonModal keepContentsMounted={true}>
      <IonDatetime
        id="date"
        onIonChange={(e) => {
          console.log(e.detail.value);
          if (typeof e.detail.value === 'string') {
            props.onChange(e.detail.value.split('T')[0]);
          } else {
            console.error('Invalid date payload');
          }
        }}
        presentation="date"
        showDefaultButtons={true}
        defaultValue={props.defaultValue}
      />
    </IonModal>
  </>
}
