import { IonApp, IonRouterOutlet, IonSplitPane, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Redirect, Route } from 'react-router-dom';
import { Menu } from './components/Menu';
import { ScheduleTimePage } from './pages/ScheduleTimePage';
import { LoginPage } from './pages/Login';
import { CreateAnAccountPage } from './pages/CreateAnAccountPage';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import { Routes } from './Routes';
import { AuthProvider } from './core/auth';
import { ProviderScheduleAdminPage } from './pages/ProviderScheduleAdminPage';
import { MyAppointmentsPage } from './pages/MyAppointmentsPage';
import { LogoutPage } from './pages/Logout';

setupIonicReact();

const App: React.FC = () => {
  return (
    <AuthProvider>
      <IonApp>
        <IonReactRouter>
          <IonSplitPane contentId="main">
            <Menu />
            <IonRouterOutlet id="main">
              <Route path="/" exact={true}>
                <Redirect to={Routes.Login} />
              </Route>
              <Route path={Routes.CreateAnAccount} exact={true}>
                <CreateAnAccountPage />
              </Route>
              <Route path={Routes.Login} exact={true}>
                <LoginPage />
              </Route>
              <Route path={Routes.Logout} exact={true}>
                <LogoutPage />
              </Route>
              <Route path={Routes.CreateAnAccount} exact={true}>
                <CreateAnAccountPage />
              </Route>
              <Route path={Routes.ScheduleTime} exact={true}>
                <ScheduleTimePage />
              </Route>
              <Route path={Routes.MyAppointments} exact={true}>
                <MyAppointmentsPage />
              </Route>
              <Route path={Routes.ProviderScheduleAdmin} exact={true}>
                <ProviderScheduleAdminPage />
              </Route>
            </IonRouterOutlet>
          </IonSplitPane>
        </IonReactRouter>
      </IonApp>
    </AuthProvider>
  );
};

export default App;
