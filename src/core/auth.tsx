import React, { createContext, useContext, useState } from 'react';
import { AccountsApi } from '../api/accounts';
import { Account } from '../api/models';

const updateLocalStorageUser = (user: Account | null) => localStorage.setItem('henrymeds_user', JSON.stringify(user));
const getLocalStorageUser = () => JSON.parse(localStorage.getItem('henrymeds_user') || 'null') as Account | null;

const useProvideAuth = () => {
  const [currentUser, setCurrentUser] = useState<Account | null>(getLocalStorageUser());

  /**
   * Clears the auth session.
   */
  const logout = () => {
    setCurrentUser(null);
    updateLocalStorageUser(null);
  }

  /**
   * Creates a new auth session
   * 
   * @returns true if successful login, false if not.
   */
  const login = async (username: string, password: string): Promise<Account | null> => {
    const user = await AccountsApi.getInstance().login(username, password);

    setCurrentUser(user);
    updateLocalStorageUser(user);

    return user;
  }

  return {
    currentUser,
    login,
    logout,
  };
}

const authContext = createContext<ReturnType<typeof useProvideAuth>>({
  currentUser: null,
  login: async () => null,
  logout: () => { },
});

export const AuthProvider: React.FC<React.PropsWithChildren<{}>> = ({ children }) => {
  const auth = useProvideAuth();
  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

export const useAuth = () => {
  return useContext(authContext);
};
