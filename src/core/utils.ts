import { Appointment, TimeSlot } from '../api/models';

/**
 * A mechanism to make async actions feel a little more real.
 */
export const wait = (timeInMs: number = 500) => new Promise((resolve) => setTimeout(resolve, timeInMs));

/**
 * Build a user-friendly time range. Probably best off using a date-fns utility for this long-term.
 */
const hourMinutesToString = (hour: number, minute: number) => {
  const minuteString = minute.toString().padStart(2, '0')
  let hourString = hour.toString().padStart(2, '0')
  let amOrPm: 'am' | 'pm' = 'am';

  if (hour === 0) {
    hourString = '12';
  }
  if (hour > 12) {
    hourString = (hour - 12).toString().padStart(2, '0');
    amOrPm = 'pm';
  }

  return `${hourString}:${minuteString} ${amOrPm}`;
}

/**
 * For the minutes/hours, ensure we have two digits.
 */
export const timeSlotToString = (timeSlot: TimeSlot) => {
  const start = hourMinutesToString(timeSlot.startHour, timeSlot.startMinute);
  const end = hourMinutesToString(timeSlot.endHour, timeSlot.endMinute);
  return `${timeSlot.date} from ${start} - ${end}`;
}

/**
 * Compare two time slots for equality.
 */
export const areTimeSlotsEqual = (a: TimeSlot | undefined, b: TimeSlot | undefined) => {
  return a?.startHour === b?.startHour
    && a?.startMinute === b?.startMinute
    && a?.endHour === b?.endHour
    && a?.endMinute === b?.endMinute;
};

/**
 * Appointment can be confirmed, pending confirmation, or autocanceled due to
 * taking more than 30 minutes to confirm.
 */
export const getAppointmentStatus = (appointment: Appointment) => {
  const threshold = 60_000 * 30; // 30 minutes

  if (appointment.confirmedAt === null) {
    console.log(appointment.scheduledAt)
    if (Date.now() - appointment.scheduledAt > threshold) {
      return 'autocanceled';
    }
    return 'pending';
  } else {
    return 'confirmed';
  }
}