import { IonButton, IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonSpinner, IonTitle, IonToolbar } from '@ionic/react';
import { useEffect, useState } from 'react';
import { Appointment, ClientAccount, ProviderTimeSlot } from '../api/models';
import { SchedulingApi } from '../api/scheduling';
import { DateSelector, getDefaultDateString } from '../components/DateSelector';
import { areTimeSlotsEqual, timeSlotToString } from '../core/utils';
import { Routes } from '../Routes';
import { useAuth } from '../core/auth';

const SelectionForm: React.FC<{ onSelection: (timeSlot: ProviderTimeSlot) => any }> = (props) => {
  const [date, setDate] = useState<string>(getDefaultDateString());
  const [status, setStatus] = useState<'loading' | 'loaded'>('loading');
  const [timeSlots, setTimeSlots] = useState<ProviderTimeSlot[] | null>(null);
  const [selectedTimeSlot, setSelectedTimeSlot] = useState<ProviderTimeSlot>();

  useEffect(() => {
    const fetchProviderAccount = async () => {
      setStatus('loading');
      try {
        const slots = await SchedulingApi.getInstance().getAvailableReservationSlots(date);
        setTimeSlots(slots);
      } finally {
        setStatus('loaded');
      }
    };
    fetchProviderAccount();
  }, [date]);

  return <div className="ion-text-center ion-margin-top">
    <h2>Select a time</h2>

    <DateSelector defaultValue={date} onChange={(date) => setDate(date)} />
    <div className="ion-margin">
      {
        status === 'loading' && (<IonSpinner color="primary" />)
      }
      {status === 'loaded' && (<>
        <div className="ion-margin ion-text-center">
          {timeSlots?.length === 0 && (<div>No time slots available for this date.</div>)}
          {timeSlots?.map((slot) => {
            const isSelected = areTimeSlotsEqual(selectedTimeSlot, slot);
            return <div>
              <IonButton color={isSelected ? 'primary' : 'light'}
                onClick={() => setSelectedTimeSlot(slot)}>
                {timeSlotToString(slot)}
              </IonButton>
              {isSelected &&
                <IonButton color="primary" disabled={!selectedTimeSlot} onClick={() => {
                  props.onSelection(selectedTimeSlot!);
                }}>
                  Submit
                </IonButton>
              }
            </div>
          })}
        </div>
      </>)}
    </div>
  </div>
}

export const ScheduleTimePage: React.FC = () => {
  const [status, setStatus] = useState<'submitted'>();
  const [savedAppointment, setSavedAppointment] = useState<Appointment | null>(null);
  const { currentUser } = useAuth();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Schedule time with a provider</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar color="primary">
            <IonTitle size="large">Schedule time with a provider</IonTitle>
          </IonToolbar>
        </IonHeader>

        {status === 'submitted'
          ? <div className="ion-text-center ion-padding">
            <h1>Submitted!</h1>
            <p>Your appointment has been scheduled. Please check your email for a verification.</p>
            <IonButton routerLink={`${Routes.MyAppointments}?confirm=${savedAppointment?.id}`}>(but since we are a demo, click here)</IonButton>
          </div>
          : <SelectionForm onSelection={async (newSelectedTimeSlot) => {
            setSavedAppointment(await SchedulingApi.getInstance().addAppointment(
              currentUser as ClientAccount,
              newSelectedTimeSlot.provider,
              newSelectedTimeSlot,
            ));
            setStatus('submitted');
          }} />
        }
      </IonContent>
    </IonPage>
  );
};

