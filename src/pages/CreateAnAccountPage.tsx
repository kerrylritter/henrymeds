import { IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';

export const CreateAnAccountPage: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Create an account</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar color="primary">
            <IonTitle size="large">Create an account</IonTitle>
          </IonToolbar>
        </IonHeader>
        <div className="ion-margin ion-text-center">
          Currently accounts can only be created by <a href="mailto:ask@henrymeds.com">contacting HenryMeds</a>.
        </div>
      </IonContent>
    </IonPage>
  );
};

