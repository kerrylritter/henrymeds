import { IonButtons, IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonMenuButton, IonPage, IonSpinner, IonText, IonTitle, IonToolbar, useIonToast } from '@ionic/react';
import { checkboxOutline, squareOutline } from 'ionicons/icons';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router';
import { AccountsApi } from '../api/accounts';
import { Appointment } from '../api/models';
import { SchedulingApi } from '../api/scheduling';
import { getAppointmentStatus, timeSlotToString } from '../core/utils';
import { useAuth } from '../core/auth';

export const MyAppointmentsPage: React.FC = () => {
  const [status, setStatus] = useState<'loading' | 'loaded'>('loading');
  const [appointments, setAppointments] = useState<Appointment[] | null>(null);
  const [present] = useIonToast();
  const { currentUser } = useAuth();
  const location = useLocation();
  const confirmId = new URLSearchParams(location.search).get('confirm');

  useEffect(() => {
    if (!currentUser) {
      return;
    }

    const fetchAppointments = async () => {
      const account = await AccountsApi.getInstance().getClient(currentUser.username);
      setAppointments(account.appointments || []);
    };

    const confirmAppointment = async () => {
      if (confirmId) {
        await SchedulingApi.getInstance().confirmAppointment(confirmId, currentUser.username);
        await present({
          message: 'Your appoinment has been confirmed!',
          duration: 1500,
          position: 'bottom'
        });
      }
      return Promise.resolve();
    }

    setStatus('loading');
    confirmAppointment()
      .then(() => fetchAppointments())
      .then(() => setStatus('loaded'));
  }, [currentUser, confirmId, present]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>My appointments</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar color="primary">
            <IonTitle size="large">My appointments</IonTitle>
          </IonToolbar>
        </IonHeader>

        {status === 'loading' && <div className="ion-text-center ion-padding"><IonSpinner /></div>}

        {status === 'loaded' && <>
          {
            appointments && appointments.length > 0 ?
              <IonList lines="full">
                {appointments.map((appointment) => {
                  const apptStatus = getAppointmentStatus(appointment);
                  return <IonItem>
                    <IonIcon icon={apptStatus === 'confirmed' ? checkboxOutline : squareOutline} slot="start" color={apptStatus === 'confirmed' ? "primary" : "dark"}></IonIcon>
                    <IonLabel>
                      <div>
                        <strong>{timeSlotToString(appointment.timeSlot)}</strong>
                      </div>
                      {apptStatus === 'confirmed' && <IonText color="primary">Confirmed</IonText>}
                      {apptStatus === 'autocanceled' && <IonText color="danger">Not confirmed, auto-canceled</IonText>}
                      {apptStatus === 'pending' && <IonText color="dark">Pending confirmation</IonText>}
                    </IonLabel>
                  </IonItem>
                })}
              </IonList>
              : <div className="ion-text-center ion-padding">You have no appointments scheduled.</div>
          }
        </>}
      </IonContent>
    </IonPage>
  );
};

