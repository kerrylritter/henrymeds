import { useEffect } from 'react';
import { useHistory } from 'react-router';
import { Routes } from '../Routes';
import { useAuth } from '../core/auth';

export const LogoutPage: React.FC = () => {
  const { logout } = useAuth();
  const history = useHistory();

  useEffect(() => {
    logout();
    history.push(Routes.Login);
  }, [logout, history]);

  return <></>;
};
