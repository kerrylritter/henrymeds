import { IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonContent, IonDatetime, IonDatetimeButton, IonGrid, IonHeader, IonItem, IonList, IonMenuButton, IonModal, IonPage, IonRow, IonSpinner, IonText, IonTitle, IonToolbar } from '@ionic/react';
import { useEffect, useState } from 'react';
import { AccountsApi } from '../api/accounts';
import { TimeSlot } from '../api/models';
import { SchedulingApi } from '../api/scheduling';
import { timeSlotToString } from '../core/utils';
import { useAuth } from '../core/auth';

const AddScheduleCard: React.FC<{ onAdd: (timeSlot: TimeSlot) => any }> = (props) => {
  const [date, setDate] = useState<string>(new Date().toISOString());

  /**
   * Only the time portion is used, so ignore the date.
   */
  const [start, setStart] = useState<string>('1970-01-01T08:00:00-00:00');
  const [end, setEnd] = useState<string>('1970-01-01T17:00:00-00:00');

  const errors = [];
  if (!date) {
    errors.push('Date is required');
  }
  if (new Date(end || '').getTime() < new Date(start || '').getTime()) {
    errors.push('Start time must be before end time');
  }

  return <>
    <IonCard>
      <IonCardHeader>
        <IonCardTitle>Add new schedule availability</IonCardTitle>
      </IonCardHeader>
      <IonCardContent>
        <IonGrid>
          <IonRow class="ion-justify-content-center">
            <IonDatetimeButton datetime="date"></IonDatetimeButton>
            <IonDatetimeButton datetime="start"></IonDatetimeButton>
            <IonDatetimeButton datetime="end"></IonDatetimeButton>
          </IonRow>
        </IonGrid>
        {
          !!errors.length && <div className="ion-padding-top ion-text-center">
            <IonText color="danger">{errors.join(' ')}</IonText>
          </div>
        }
        <div className="ion-padding-top">
          <IonButton
            color="primary"
            expand="block"
            disabled={!!errors.length}
            onClick={() => {
              const dateDateString = date.split('T')[0];
              const startTimeString = start.split('T')[1].split('-')[0];
              const startTimeSplits = startTimeString.split(':');
              const endTimeString = end.split('T')[1].split('-')[0];
              const endTimeSplits = endTimeString.split(':');

              const newTimeSlot: TimeSlot = {
                date: dateDateString,
                startHour: parseInt(startTimeSplits[0], 10),
                startMinute: parseInt(startTimeSplits[1], 10),
                endHour: parseInt(endTimeSplits[0], 10),
                endMinute: parseInt(endTimeSplits[1], 10),
              };

              props.onAdd(newTimeSlot);

              setDate(new Date().toISOString());
              setStart('1970-01-01T08:00:00-00:00');
              setEnd('1970-01-01T17:00:00-00:00');
            }}>
            Add
          </IonButton>
        </div>
      </IonCardContent>
    </IonCard>

    <IonModal keepContentsMounted={true}>
      <IonDatetime
        id="date"
        onIonChange={(e) => typeof e.detail.value === 'string' && setDate(e.detail.value)}
        presentation="date"
        showDefaultButtons={true}
        value={date}
      />
    </IonModal>
    <IonModal keepContentsMounted={true}>
      <IonDatetime
        id="start"
        onIonChange={(e) => typeof e.detail.value === 'string' && setStart(e.detail.value)}
        presentation="time"
        showDefaultButtons={true}
        value={start}
      />
    </IonModal>
    <IonModal keepContentsMounted={true}>
      <IonDatetime
        id="end"
        onIonChange={(e) => typeof e.detail.value === 'string' && setEnd(e.detail.value)}
        presentation="time"
        showDefaultButtons={true}
        value={end}
      />
    </IonModal>
  </>
}

const ScheduleList: React.FC<{ timeSlots: TimeSlot[] }> = (props) => {
  if (!props.timeSlots?.length) {
    return <div className="ion-text-center">You have no time slots scheduled.</div>;
  }

  return <IonList lines="full">
    {props.timeSlots.map((timeSlot) => {
      return <IonItem>
        <strong>
          {timeSlotToString(timeSlot)}
        </strong>
      </IonItem>
    })}
  </IonList>
}

export const ProviderScheduleAdminPage: React.FC = () => {
  const [status, setStatus] = useState<'loading' | 'loaded'>('loading');
  const [timeSlots, setTimeSlots] = useState<TimeSlot[] | null>(null);
  const { currentUser } = useAuth();

  const updateAvailability = async (timeSlot: TimeSlot) => {
    if (!currentUser) {
      return;
    }

    setStatus('loading');
    try {
      const providerAccount = await SchedulingApi.getInstance().addProviderAvailability(currentUser!.username, timeSlot);
      setTimeSlots(providerAccount.schedule);
    } finally {
      setStatus('loaded');
    }
  }

  useEffect(() => {
    const fetchProviderAccount = async () => {
      if (!currentUser) {
        return;
      }

      setStatus('loading');
      try {
        const providerAccount = await AccountsApi.getInstance().getProvider(currentUser.username);
        setTimeSlots(providerAccount.schedule);
      } finally {
        setStatus('loaded');
      }
    };

    fetchProviderAccount();
  }, [currentUser]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Schedule time with a provider</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar color="primary">
            <IonTitle size="large">Schedule time with a provider</IonTitle>
          </IonToolbar>
        </IonHeader>

        <AddScheduleCard onAdd={(timeSlot) => updateAvailability(timeSlot)} />

        {status === 'loading' && <div className="ion-text-center ion-padding"><IonSpinner /></div>}
        {status === 'loaded' && <div className="ion-text-center ion-padding"><ScheduleList timeSlots={timeSlots || []} /></div>}

      </IonContent>
    </IonPage>
  );
};

