import { IonButton, IonButtons, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonList, IonMenuButton, IonPage, IonSpinner, IonTitle, IonToolbar, useIonToast } from '@ionic/react';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { Routes } from '../Routes';
import { useAuth } from '../core/auth';

export const LoginPage: React.FC = () => {
  const [credentials, setCredentials] = useState({ username: '', password: '' });
  const [status, setStatus] = useState<'loading' | null>(null);
  const [present] = useIonToast();
  const { currentUser, login } = useAuth();
  const history = useHistory();

  const handleLogin = async () => {
    setStatus('loading');
    try {
      const loginResult = await login(credentials.username, credentials.password);
      if (!loginResult) {
        await present({
          message: 'The provided credentials were not correct.',
          duration: 1500,
          position: 'bottom'
        });
      }
    } catch {
      await present({
        message: 'Something went wrong! Please try again.',
        duration: 1500,
        position: 'bottom'
      });
    } finally {
      setStatus(null);
    }
  };

  const handleInput = (key: 'username' | 'password', target: HTMLIonInputElement) => {
    const value = target.value?.toString() || '';

    setCredentials((currentCredentials) => {
      return {
        ...currentCredentials,
        [key]: value
      }
    })
  };

  useEffect(() => {
    if (currentUser?.type === 'client') {
      history.push(Routes.ScheduleTime);
    } else if (currentUser?.type === 'provider') {
      history.push(Routes.ProviderScheduleAdmin);
    }
  }, [currentUser, history]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar color="primary">
            <IonTitle size="large">Login</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonList inset={true} lines="full">
          <IonItem>
            <IonLabel position="fixed">Username</IonLabel>
            <IonInput
              onIonChange={(ev) => handleInput('username', ev.target)}
              onKeyDown={(ev) => ev.key === 'Enter' && handleLogin()}
            />
          </IonItem>
          <IonItem>
            <IonLabel position="fixed">Password</IonLabel>
            <IonInput
              type="password"
              onIonChange={(ev) => handleInput('password', ev.target)}
              onKeyDown={(ev) => ev.key === 'Enter' && handleLogin()}
            />
          </IonItem>
          <IonButton
            expand="block"
            onClick={() => handleLogin()}
            disabled={status === 'loading'}>
            {status === 'loading' ? <IonSpinner /> : 'Login'}
          </IonButton>
        </IonList>
      </IonContent>
    </IonPage>
  );
};
