import { wait } from '../core/utils';
import { Database } from './database';
import { ClientAccount, ProviderAccount } from './models';

export class AccountsApi {
  private static instance: AccountsApi;

  public static getInstance(): AccountsApi {
    if (!AccountsApi.instance) {
      AccountsApi.instance = new AccountsApi();
    }

    return AccountsApi.instance;
  }

  private database = Database.getInstance();

  async login(username: string, _password: string) {
    await wait();

    return this.database.accounts.find(a => a.username === username) || null;
  }

  async getProviders(): Promise<ProviderAccount[]> {
    await wait();

    return this.database.accounts.filter(a => a.type === 'provider') as ProviderAccount[];
  }

  async getProvider(username: string): Promise<ProviderAccount> {
    await wait();

    return this.database.accounts.find(a => a.type === 'provider' && a.username === username) as ProviderAccount;
  }

  async getClient(username: string): Promise<ClientAccount> {
    await wait();

    return this.database.accounts.find(a => a.type === 'client' && a.username === username) as ClientAccount;
  }
}