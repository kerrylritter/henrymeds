
/**
 * Hours are in 24-hour format.
 */
export type TimeSlot = {
  /**
   * Date in YYYY-MM-DD format.
   */
  date: string;

  /**
   * Hours are in 24-hour format.
   */
  startHour: number;

  /**
   * Minutes are in 60-minute format.
   */
  startMinute: number;

  /**
   * Hours are in 24-hour format.
   */
  endHour: number;

  /**
   * Minutes are in 60-minute format.
   */
  endMinute: number;
};

export type ProviderTimeSlot = TimeSlot & { provider: ProviderAccount };

export type TimeSlotStrings = {
  date: string;
  start: string;
  end: string;
}

export type Appointment = {
  id: string;
  clientUsername: string;
  providerUsername: string;
  timeSlot: TimeSlot;
  scheduledAt: number;
  confirmedAt: number | null;
}

type AccountBase = { username: string; }

export type ProviderAccount = AccountBase & {
  type: 'provider';
  schedule: TimeSlot[];
  appointments: Appointment[];
}

export type ClientAccount = AccountBase & {
  type: 'client';
  appointments: Appointment[];
}

export type Account = (ProviderAccount | ClientAccount)