import { wait } from '../core/utils';
import { Account } from './models';

export class Database {
  private static instance: Database;

  public static getInstance(): Database {
    if (!Database.instance) {
      Database.instance = new Database();
    }

    return Database.instance;
  }

  public accounts: Account[] = [];

  constructor() {
    this.accounts = this.initializeFromLocalStorage();
  }

  async updateAccount(username: string, account: Account) {
    await wait();
    this.accounts = this.accounts.map(a => a.username === username ? account : a);
    this.updateLocalStorage(this.accounts);
  }

  private initializeFromLocalStorage() {
    let jsonData = localStorage.getItem('henrymeds');
    if (!jsonData) {
      this.updateLocalStorage([
        {
          username: 'provideruser',
          type: 'provider',
          schedule: ['2023-03-20', '2023-03-21', '2023-03-22', '2023-03-23', '2023-03-24', '2023-03-25', '2023-03-26', '2023-03-27'].map(date => {
            return {
              date,
              startHour: 8,
              startMinute: 0,
              endHour: 17,
              endMinute: 0
            };
          }),
          appointments: []
        },
        {
          username: 'clientuser',
          type: 'client',
          appointments: []
        },
      ]);
      jsonData = localStorage.getItem('henrymeds')!;
    }

    return JSON.parse(jsonData) as Account[];
  }

  private updateLocalStorage(accounts: Account[]) {
    localStorage.setItem('henrymeds', JSON.stringify(accounts));
  }
}