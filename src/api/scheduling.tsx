import { areTimeSlotsEqual, wait } from '../core/utils';
import { AccountsApi } from './accounts';
import { Database } from './database';
import { Appointment, ClientAccount, ProviderAccount, ProviderTimeSlot, TimeSlot } from './models';

export class SchedulingApi {
  private static instance: SchedulingApi;

  public static getInstance(): SchedulingApi {
    if (!SchedulingApi.instance) {
      SchedulingApi.instance = new SchedulingApi();
    }

    return SchedulingApi.instance;
  }

  private accountsApi = AccountsApi.getInstance();
  private database = Database.getInstance();

  async getAvailableReservationSlots(date: string): Promise<ProviderTimeSlot[]> {
    await wait();

    const providers = await this.accountsApi.getProviders();

    const timeSlots: ProviderTimeSlot[] = [];

    for (const provider of providers) {
      const providerTimeSlotsForDate = this.getProviderAvailability(provider, date);
      for (const timeslot of providerTimeSlotsForDate) {
        if (!timeSlots.some((slot) => slot.startHour === timeslot.startHour && slot.startMinute === timeslot.startMinute)) {
          timeSlots.push({ ...timeslot, provider });
        }
      }
    }

    return timeSlots;
  }

  /**
   * Adds an appointment to the database and returns the appointment.
   */
  async addAppointment(client: ClientAccount, provider: ProviderAccount, providerTimeSlot: ProviderTimeSlot): Promise<Appointment> {
    await wait();

    const timeSlot: TimeSlot = { ...providerTimeSlot };

    if ("provider" in timeSlot) {
      delete timeSlot.provider;
    }

    const appointment: Appointment = {
      id: Math.random().toString(36).substr(2, 9), // Replace with uuid
      clientUsername: client.username,
      providerUsername: provider.username,
      scheduledAt: new Date().getTime(),
      confirmedAt: null,
      timeSlot,
    }

    provider.appointments.push(appointment);
    client.appointments.push(appointment);

    await Promise.all([
      this.database.updateAccount(provider.username, provider),
      this.database.updateAccount(client.username, client),
    ]);

    return appointment;
  }

  /**
   * Updates the appointment confirmation date and returns the appointment.
   */
  async confirmAppointment(confirmId: string, clientUsername: string) {
    const client = await this.accountsApi.getClient(clientUsername);
    const appointment = client.appointments.find(a => a.id === confirmId)!;

    const provider = await this.accountsApi.getProvider(appointment.providerUsername);
    appointment.confirmedAt = new Date().getTime();

    client.appointments = client.appointments.map(a => a.id === appointment.id ? appointment : a);
    provider.appointments = provider.appointments.map(a => a.id === appointment.id ? appointment : a);

    await Promise.all([
      this.database.updateAccount(provider.username, provider),
      this.database.updateAccount(client.username, client),
    ]);
  }

  /**
   * Adds a time slot to the provider's schedule and returns the provider.
   */
  async addProviderAvailability(providerUsername: string, timeSlot: TimeSlot) {
    const provider = await this.accountsApi.getProvider(providerUsername);
    if (provider.schedule.find(s => areTimeSlotsEqual(s, timeSlot))) {
      return provider;
    }
    provider.schedule.push(timeSlot);
    provider.schedule = provider.schedule.sort((a, b) => a.date.localeCompare(b.date));
    await this.database.updateAccount(providerUsername, provider);
    return provider;
  }

  private getProviderAvailability(provider: ProviderAccount, date: string): TimeSlot[] {
    const timeSlotsForDate = provider.schedule.find(s => s.date === date);
    if (!timeSlotsForDate) {
      return [];
    }

    const startMinutes = timeSlotsForDate.startHour * 60 + timeSlotsForDate.startMinute;
    const endMinutes = timeSlotsForDate.endHour * 60 + timeSlotsForDate.endMinute;

    const timeSlots: TimeSlot[] = [];

    for (let i = startMinutes; i < endMinutes; i += 15) {
      const hour = Math.floor(i / 60);
      const minute = i % 60;

      if (hour < timeSlotsForDate.startHour || (hour === timeSlotsForDate.startHour && minute < timeSlotsForDate.startMinute)) {
        continue;
      }

      if (hour > timeSlotsForDate.endHour || (hour === timeSlotsForDate.endHour && minute >= timeSlotsForDate.endMinute)) {
        break;
      }

      let endHour = hour;
      let endMinute = minute + 15;

      if (endMinute >= 60) {
        endHour++;
        endMinute = 0;
      }

      const hasConflict = provider.appointments.some((appointment) => {
        return appointment.timeSlot.startHour === hour && appointment.timeSlot.startMinute === minute;
      });

      if (hasConflict) {
        continue;
      };

      timeSlots.push({
        date,
        startHour: hour,
        startMinute: minute,
        endHour,
        endMinute,
      });
    }

    return timeSlots;
  }
}

