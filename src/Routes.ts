export const Routes = {
  Login: '/login',
  Logout: '/logout',
  CreateAnAccount: '/create-an-account',
  ScheduleTime: '/schedule-an-appointment',
  MyAppointments: '/my-appointments',
  ProviderScheduleAdmin: '/provider-schedule-admin',
} as const;
