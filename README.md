# Reservations UI

<hr />

## Getting Started

Preqrequisites: Node 16+

1. Run `npm install` in the root directory.
2. Run `npm run start`.
3. Go to `localhost:3000` in your browser.
4. Login with `clientuser` as a client and `provideruser` as a provider. Any password is accepted.

## Supported features

- Providers
  - Manage their availability on a day-to-day basis.
- Clients
  - View their reservation records for all given statuses.
  - Make a reservation.
  - Confirm a reservation given a confirmation code/URL.

## Notes and Future considerations

- I used Ionic Framework's React implementation for the UI. I've used this framework in the past and it's a great way to get a mobile-friendly UI up and running quickly.
- I typically don't build out API clients like I do in this example and much prefer to leverage auto-generated clients (i.e. via OpenAPI or GraphQL tooling). This would clean up a bit of code.
- Most of the work done in the APIs should be handled by the server-side (i.e. the API should do the calculation to determine available time slots given schedules and scheduled appointments).
- Some key validations I skipped over due to time are ensuring all date ranges are valid, all time slots are valid, and selected appointment times are valid.
- I did not implement a state store for this example as its size/complexity didn't particularly warrant it, but would typically use something like Redux.
- Error handling is largely disregarded, but can easily be managed by error boundaries or toast notifications on failures.
